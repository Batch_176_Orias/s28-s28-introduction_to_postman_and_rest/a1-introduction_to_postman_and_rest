let server = require("express");

let port = 4000;

let application = server();

//to receive data from server body
application.use(server.json());

let to_do = [];

application.listen(port, () => {
	console.log(`Server ${port} is now running`);
});

//GET

application.get(`/list`, (req, res)=>{
	if (to_do.length > 0) {
		/*res.send(`The collection is not empty: ${JSON.stringify(to_do.length)} count(s)`);*/
		res.send(to_do);
	} 
	else {
		res.send(`The collection is empty`);
	}	
});

//GET Pending

application.get(`/pending`, (req, res)=>{
	if (to_do.length > 0) {
		/*res.send(`The collection is not empty: ${JSON.stringify(to_do.length)} count(s)`);*/
		let newTask = to_do.filter((task)=>{
			return task.status === `pending`;
		});
		res.send(newTask);
	} 
	else {
		res.send(`The collection is empty`);
	}
});

//POST

application.post(`/list`, (req, res)=>{
	let taskName = req.body.name;
	let feedback="";
	let length;

	if(req.body.name == "" || req.body.status == ""){
		feedback = `All fields required`;
	}
	else{
		if(to_do.length === 0){
			feedback = `Task ${taskName} added`;
			to_do.push(req.body);
		}
		else {
				let newTask = to_do.filter((task)=>{
					return task.name === taskName;
				});
				console.log(`${JSON.stringify(newTask)}`);
				console.log(newTask.length);

				length = newTask.length;

				if(length == 0){
					feedback = `Task ${taskName} added`;
					to_do.push(req.body);
				}
				else{
					feedback = `Task ${taskName} existing`;
				}

			/*if(newTask.name == req.body.name){
				console.log(`Record exist`);
				newTask ="";
				console.log(newTask.length);
			}
			else{
				console.log(`record will be added`);
				to_do.push(req.body);
			}*/


			/*let rec = to_do.filter((task)=>{
				if(task.name === taskName){
					return (task)
				}
				else{
					return `Not exisiting`
				}
			});
			feedback = rec;*/
		}
	}
	
	
	res.send(feedback);

});

//PUT STATUS

application.put(`/list_status`, (req, res)=>{
	let feedback;
	if(req.body.name !== "" && req.body.status !== ""){
		if (to_do.length > 0) {
			for(let i = 0; i < to_do.length; i++){
				if (req.body.name === to_do[i].name) {
					feedback = `Task ${req.body.name} updated`;
					to_do[i].status = req.body.status;
					break;
				} 
				else {
					feedback = `Task ${req.body.name} not found`;
				}
			}
		} 
		else {
			feedback = `The collection is empty`;
		}

	}
	else{
		feedback = `All fields required`;
	}

	res.send(feedback);
});

//PUT NAME

application.put(`/list_name`, (req, res)=>{
let feedback;
let length;
if(req.body.name !== "" && req.body.name1 !== ""){
	/*if(to_do.length > 0){
		let newTask = to_do.filter((task)=>{
			return task.name === req.body.name1;
		})
		console.log(`PUT ${JSON.stringify(newTask)}`);
		console.log(`PUT ${newTask.length}`);

		length = newTask.length;

		if(length === 0){
			feedback = `Task neme ${taskName} updated to: ${req.body.name}`;
			newTask.name = req.body.name;
		}
		else{
			feedback = `Task name ${req.body.name} already existing`;
		}
	}
	else{
		feedback = `The collection is empty`;
	}*/
	if (to_do.length > 0) {
		for(let i = 0; i < to_do.length; i++){
			if (req.body.name === to_do[i].name) {
				/*feedback = `Task ${req.body.name} exist`;
				break;*/
				let newTask = to_do.filter((task)=>{
					return task.name === req.body.name1;
				});
				console.log(`PUT ${JSON.stringify(newTask)}`);
				console.log(`PUT ${newTask.length}`);

				length = newTask.length;

				if(length == 0){
					feedback = `Task name ${req.body.name} updated to: ${req.body.name1}`;
					to_do[i].name = req.body.name1;
					break;
				}
				else{
					feedback = `Task name ${req.body.name} can not be updated to task name: ${req.body.name1}. Task name already exists`;
					break;
				}
			} 
			else {
				feedback = `Task ${req.body.name} not found`;
			}
		}
	} 
	else {
		feedback = `The collection is empty`;
	}
}
else{
	feedback = `All fields required`;
}

	res.send(feedback);

});


//DELETE

application.delete(`/list`, (req, res)=>{
	let feedback;
	if(req.body.name !== ""){
		if (to_do.length > 0) {
			for(let i = 0; i < to_do.length; i++){
				if (req.body.name === to_do[i].name) {
					feedback = `Record ${req.body.name} deleted`;
					to_do.splice(i, 1);
					break;
				} 
				else {
					feedback = `Task ${req.body.name} not found`;
				}
			}
		} 
		else {
			feedback = `The collection is empty`;
		}

	}
	else{
		feedback = `All fields required`;
	}

	res.send(feedback);
});

